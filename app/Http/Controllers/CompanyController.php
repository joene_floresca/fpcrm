<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Redirect;
use Session;
use Input;
use View;
use App\Http\Models\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
	{
		return view('company.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  
	public function store(Request $request)
	{
		$rules = array(
            'company_name'  			 => 'required',
            'address'  	 				 => 'required',
            'country'  	 		         => 'required',
            'ceo_name'  	             => 'required',
            'contact_person'  	         => 'required',
            'phone_number'  	 	     => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('company/create')->withErrors($validator);
        }
        else
        {
        	$company = new Company;
            $company->company_name 			= Input::get('company_name');
			$company->address 			    = Input::get('address');
			$company->country 			    = Input::get('country');
			$company->ceo_name 			    = Input::get('ceo_name');
			$company->contact_person 	    = Input::get('contact_person');
			$company->phone_number 			= Input::get('phone_number');
            $company->save();

            Session::flash('alert-success', 'Form Submitted Successfully.');

            return Redirect::to('company/create');
        }
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
	

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
		return View::make('company.edit')->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  	
	
	public function update(Request $request, $id)
	{
		$rules = array(
            'company_name'           => 'required',
			'ceo_name'               => 'required',
			'phone_number'           => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) 
        {
            return Redirect::to('company')->withErrors($validator);
        }
        else
        {
            $company = Company::find($id);
            $company->company_name           = Input::get('company_name');
			$company->address                = Input::get('address');
			$company->ceo_name               = Input::get('ceo_name');
			$company->contact_person         = Input::get('contact_person');
			$company->phone_number           = Input::get('phone_number');
            $company->save();
           
            Session::flash('alert-success', 'Company Updated Successfully.');

            return Redirect::to('company');
        }
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	 public function getCompanyAll()
    {
       return json_encode(Company::all());
    }
	
	
}
