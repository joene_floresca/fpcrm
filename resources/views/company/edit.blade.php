@extends('master')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading"><i class="fa fa-building" id="panel-icon"></i>Edit Company</div>
				<div class="panel-body">
				
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
					{!! Form::model($company, array('route' => array('company.update', $company->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Company Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="company_name" id="company_name" value="{{ $company->company_name }}">
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="address" id="address" value="{{ $company->address }}">
							</div>
						</div>
						
					
						<div class="form-group">
							<label class="col-md-4 control-label">CEO Name</label>
							<div class="col-md-6">
						
								<input type="text" class="form-control" name="ceo_name" id="ceo_name" value="{{ $company->ceo_name }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Contact Person</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="contact_person" id="contact_person" value="{{ $company->contact_person }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Phone Number</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phone_number" id="phone_number" value="{{ $company->contact_person }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Edit
								</button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
