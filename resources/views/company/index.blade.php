@extends('master')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading"><i class="fa fa-building" id="panel-icon"></i>Company List</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
					<div class="loading-progress" id="progressbar" style="padding-left: 2px; padding-right: 2px; padding-top: 2px"></div>
                    <table id="companyList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Country</th>
                                <th>CEO Name</th>
                                <th>Phone Number</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('company-index')
<script>
//$.ajax({
//	url: "get-company",
//	type: 'GET',
//	success: function(result){
//	var myObj = $.parseJSON(result);
//			$.each(myObj, function(key,value) {
//				var t = $('#companyList').DataTable();
//
//				t.row.add( [
//							value.company_name,
//							value.country,
//							value.ceo_name,
//							value.phone_number,
//						
//					] ).draw();
//
//		});
//	}});
$.ajax({
		url: "get-company",
		type: 'GET',
		success: function(result){
		var myObj = $.parseJSON(result);
	    	$.each(myObj, function(key,value) {
	    		var t = $('#companyList').DataTable();

	    		t.row.add( [
		                value.company_name,
						value.country,
						value.ceo_name,
						value.phone_number,
					 "<a class='btn btn-small btn-info' href='<?php echo URL::to('company').'/';?>"+value.id+"/edit'><span class='glyphicon glyphicon glyphicon-edit' aria-hidden='true'></span></a>",
		            "<form method='POST' action='<?php echo URL::to('company').'/';?>"+value.id+"' accept-charset='UTF-8' class='pull-left' >"+
		            "<input name='_method' type='hidden' value='DELETE'>"+
		            "<button type='submit' class='btn btn-warning'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>"+"</form>",
	        	] ).draw();

			});
		}})	;
</script>
@endsection
